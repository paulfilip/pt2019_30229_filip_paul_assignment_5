package PT2019.Assignment_5;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

public class TextFileWriter {
	public static void writeNumberOccurences(HashMap<String, Integer> hashMap) throws IOException {
		FileWriter fileWriter = new FileWriter("ActivitesOccurence.txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);

		Set<Entry<String, Integer>> hashSet = hashMap.entrySet();
		for (@SuppressWarnings("rawtypes")
		Entry entry : hashSet) {
			printWriter.println(entry.getKey() + " " + entry.getValue());
		}
		printWriter.close();
	}

	public static void writeOccurenceEachDay(HashMap<Integer, HashMap<String, Integer>> hashMapEachDay)
			throws IOException {
		FileWriter fileWriter = new FileWriter("ActivitesOccuranceForEachDay.txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);

		Set<Entry<Integer, HashMap<String, Integer>>> hsEachDay = hashMapEachDay.entrySet();
		for (@SuppressWarnings("rawtypes")
		Entry entry : hsEachDay) {
			printWriter.println("Day " + entry.getKey());
			@SuppressWarnings("unchecked")
			Set<Entry<String, Integer>> hashSetIn = (Set<Entry<String, Integer>>) ((HashMap<String, Integer>) entry
					.getValue()).entrySet();
			for (@SuppressWarnings("rawtypes")
			Entry entry1 : hashSetIn) {
				printWriter.println("	" + entry1.getKey() + " " + entry1.getValue());
			}
		}

		printWriter.close();
	}

	public static void durationEachLine(ArrayList<MonitoredData> activities) throws IOException {
		FileWriter fileWriter = new FileWriter("EachActivityLabelDuration.txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);
		activities.stream().forEach(monData -> {
			long recordTimeInMillis = monData.getEndTime().getTime() - monData.getStartTime().getTime();
			Time timeForThisRecord = new Time(recordTimeInMillis - TimeUnit.HOURS.toMillis(2));
			printWriter.println(monData.toString() + "\t\tDuration: " + timeForThisRecord.toString());
		});
		printWriter.close();
	}

	public static void totalMonitoredTime(HashMap<String, Long> hashMapTime) throws IOException {
		FileWriter fileWriter = new FileWriter("TotalRecordEachActivity.txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);

		Set<Entry<String, Long>> hsRecord = hashMapTime.entrySet();
		for (@SuppressWarnings("rawtypes")
		Entry entry : hsRecord) {
			long timeInMillis = (long) entry.getValue();

			long seconds = timeInMillis / 1000;
			long minutes = seconds / 60;
			long hours = minutes / 60;
			long days = hours / 24;

			long totalHours = hours % 24;
			long totalMinutes = minutes % 60;
			long totalSeconds = seconds % 60;

			printWriter.println(entry.getKey() + "-> " + days + " days, " + totalHours + " hours, " + totalMinutes
					+ " minutes, " + totalSeconds + " seconds");
		}
		printWriter.close();
	}
}
