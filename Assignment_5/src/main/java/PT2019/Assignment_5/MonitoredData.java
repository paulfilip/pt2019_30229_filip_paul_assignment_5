package PT2019.Assignment_5;

import java.util.Calendar;
import java.util.Date;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activity;

	public MonitoredData(Date startTime, Date endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getActivity() {
		return activity;
	}

	@Override
	public String toString() {
		return "Start: "+startTime + "	" + "End: "+endTime +"	"+"Activity: "+activity;
	}
	
	public boolean hasSameDay(MonitoredData data){
		boolean result;
		Calendar date1 = Calendar.getInstance();
		Calendar date2 = Calendar.getInstance();
		date1.setTime(this.getStartTime());
		date2.setTime(data.getStartTime());
		
		if(date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) && date1.get(Calendar.DAY_OF_MONTH) == date2.get(Calendar.DAY_OF_MONTH) && date1.get(Calendar.MONTH) == date2.get(Calendar.MONTH))
			result = true;
		else
			result = false;
		//System.out.println(this.toString() + "\n"+ data.toString() + "\n=>"+result);
		return result;
	}
	
	public long differenceInMillis(){
		long result;
		result = Math.abs(this.getEndTime().getTime() - this.getStartTime().getTime());
		return result;
	}
}
